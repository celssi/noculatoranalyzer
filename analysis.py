import matplotlib
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import matplotlib.animation as animation

matplotlib.use("Agg")


class NoculatorAnalyzer:
    mapp_df = pd.read_csv('running_core_per_src.csv')
    index = int(mapp_df.size / len(mapp_df.columns))
    mapp_row = mapp_df.iloc[index - 1]

    colors = {-1: 'black', 0: 'blue', 1: 'yellow', 2: 'skyblue', 3: 'lime', 4: 'darkorange', 5: 'red', 6: 'saddlebrown', 7: 'olive'}
    blue_patch = mpatches.Patch(color='blue', label='ferret')
    yellow_patch = mpatches.Patch(color='yellow', label='blackshols')
    skyblue_patch = mpatches.Patch(color='skyblue', label='fluidanimate')
    lime_patch = mpatches.Patch(color='lime', label='bodytrack')
    darkorange_patch = mpatches.Patch(color='darkorange', label='facesim')
    red_patch = mpatches.Patch(color='red', label='streamcluster')
    saddlebrown_patch = mpatches.Patch(color='saddlebrown', label='caneal')

    applications = {0: 'blackshols', 1: 'bodytrack', 2: 'caneal', 3: 'facesim', 4: 'ferret', 5: 'fluidanimate', 6: 'streamcluster'}

    @staticmethod
    def process_power_per_source():
        df = pd.read_csv('power_per_src.csv', sep='\t')
        plt.plot(df)
        plt.title('Power per resource')
        plt.ylabel('Power')
        plt.xlabel('Execution cycles (x10000)')

        pp = PdfPages('power_per_src.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_network_average_latency():
        df = pd.read_csv('network_average_latency.csv')
        plt.plot(df)
        plt.title('Network Avg. Latency')
        plt.ylabel('Network Avg. Latency')
        plt.xlabel('Execution cycles (x10$^6$)')

        loc = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]
        label = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]
        plt.xticks(loc, label)
        plt.xlim(0, 900)

        pp = PdfPages('network_average_latency.pdf')
        pp.savefig()

        pp.close()

    @staticmethod
    def process_temperature_per_source():
        df = pd.read_csv('temperature_per_src.csv')
        plt.plot(df)
        plt.title('Temperature per resource')
        plt.ylabel('Temperature')
        plt.xlabel('Execution cycles (x1000)')

        pp = PdfPages('temperature_per_src.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_average_temperatures(self):
        df = pd.read_csv('temperature_per_src.csv')
        sum_row = df.sum()
        index = int(df.size / len(df.columns))
        avg_power = sum_row / index
        ax = avg_power.plot(kind='bar', title='Avg. temperature', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        ax.set_xlabel("Resource")
        ax.set_ylabel("Average temperature")

        pp = PdfPages('average_temperature.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_average_power_per_source(self):
        df = pd.read_csv('power_per_src.csv', sep='\t')
        sum_row = df.sum()
        index = int(df.size / len(df.columns))
        avg_power = sum_row / index
        ax = avg_power.plot(kind='bar', title='Avg. power', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        ax.set_xlabel("Resource")
        ax.set_ylabel("Average power")
        pp = PdfPages('average_power.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_deflected_flit_per_src(self):
        df = pd.read_csv('deflected_flit_per_src.csv')
        sum_row = df[0:100000].sum()

        avg_dfpki = sum_row / 100000
        ax = avg_dfpki.plot(kind='bar', title='Avg. deflected flits per kilo instruction (DFPKI)', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        plt.legend(handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch], ncol=7, loc='best')
        ax.set_xlabel("Tiles")
        ax.set_ylabel("Avg. DFPKI")
        file = open('Avg.DFPKI.txt', 'w')

        for i in range(0, 8):
            for j in range(0, 8):
                file.write("%d " % avg_dfpki[i * 8 + j])
            file.write("\n")
        file.close()

        pp = PdfPages('average_deflected_flit.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_l1_miss_rate(self):
        df = pd.read_csv('MPKI_L1.csv')
        sum_row = df[0:10000].sum()
        avg_mpki = sum_row / 10000
        ax = avg_mpki.plot(kind='bar', title='L1 avg. MPKI', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=4, loc='best')
        ax.set_xlabel("Cores")
        ax.set_ylabel("Average MPKI for L1 cache")
        plt.gcf().subplots_adjust(bottom=0.35)
        pp = PdfPages('average_mpki_l1.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_memory_access_per_kilo_instruction(self):
        df = pd.read_csv('memory_req_per_src.csv')
        sum_row = df.sum()
        index = int(df.size / len(df.columns))
        avg_mpki = sum_row / index
        ax = avg_mpki.plot(kind='bar', title='Avg. memory request', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=7, loc='best')
        ax.set_xlabel("Cores")
        ax.set_ylabel("Average memory request")

        pp = PdfPages('average_memory_request.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_memory_access_per_src(self):
        df = pd.read_csv('memory_access_per_src.csv')
        sum_row = df.sum()
        index = int(df.size / len(df.columns))
        avg_mpki = sum_row / index
        ax = avg_mpki.plot(kind='bar', title='Avg. memory access', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=7, loc='best')
        ax.set_xlabel("Cores")
        ax.set_ylabel("Average memory access")

        pp = PdfPages('average_memory_access.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_core_progress_log_retired(self):
        df = pd.read_csv('core_progress_log_retired.csv')
        index = int(df.size / len(df.columns))
        row = df.iloc[index - 1]
        s = row.sum()
        ax = row.plot(kind='bar', title='Number of retired instructions', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5))
        ax.set_xlabel("Cores")
        ax.set_ylabel("Number of retired instructions")
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=7, loc='best')

        pp = PdfPages('retired_instruction.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_memory_committed_instructions_per_source(self):
        df_mem = pd.read_csv('memory_commited_insts_persrc.csv')
        index = int(df_mem.size / len(df_mem.columns))
        row_mem = df_mem.iloc[index - 1]
        ax = row_mem.plot(kind='bar', title='Number commited instructions', color=[self.colors[t] for t in self.mapp_row], figsize=(20, 2.5), hatch='*')
        ax.set_xlabel("Cores")
        ax.set_ylabel("Number of commited instructions")
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=7, loc='best')

    @staticmethod
    def process_core_cache_slice_access(self):
        df = pd.read_csv('core_cacheSlice_access.csv')
        k = df.as_matrix()
        plt.figure(figsize=(10, 10))

        for i in range(0, 63):
            for j in range(0, 63):
                kk = k[i][j] / 1050
                plt.scatter(i, j, s=kk, marker='o', c=self.colors[self.mapp_row[j]])

        plt.xlim(0, 63)
        plt.ylim(0, 63)
        plt.xlabel("L2 cache slice", fontsize=18)
        plt.ylabel("Cores", fontsize=18)
        plt.xticks(fontsize=18)
        plt.yticks(fontsize=18)
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=4, loc='upper center', bbox_to_anchor=(0.5, 1.05))

        pp = PdfPages('core_cacheSlice_access.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_cache_slice_mem_access():
        df = pd.read_csv('cacheSlice_mem_access.csv')
        k = df.as_matrix()
        plt.figure(figsize=(10, 10))
        for i in range(0, 4):
            for j in range(0, 63):
                plt.scatter(i, j, s=k[i][j] / 100, marker='o')

        plt.ylim(0, 63)
        plt.xlim(-1, 4)
        plt.xticks([0, 1, 2, 3])

        plt.ylabel("L2 cache slice", fontsize=18)
        plt.xlabel("Memory bank", fontsize=18)
        plt.xticks(fontsize=18)
        plt.yticks(fontsize=18)

        pp = PdfPages('cacheSlice_mem_access.pdf')
        pp.savefig()
        pp.close()

    @staticmethod
    def process_thread_scheduling_result(self):
        df = pd.read_csv('thread_scheduling_result.csv')

        for i, s in df.iterrows():
            plt.plot([s['startTime'], s['endTime']], [s['coreID'], s['coreID']], color=self.colors[s['appID']], linewidth=0.5)
            plt.plot([s['startTime'], s['endTime']], [s['coreID'], s['coreID']], '|', markersize=3.0,
                     color=self.colors[s['appID']])

        plt.ylim(-0.5, 63.5)

        plt.xlabel('Execution cycles (x10$^6$)', size=18)
        plt.xticks(size=18)
        plt.ylabel('Core', size=18)
        plt.yticks(size=18)
        plt.legend(
            handles=[self.blue_patch, self.yellow_patch, self.skyblue_patch, self.lime_patch, self.darkorange_patch, self.red_patch, self.saddlebrown_patch],
            ncol=4, loc='upper center', fontsize=18)
        plt.title('Thread scheduling (gantt chart)', size=18)

        plt.xlim(0, 900000)

        loc = [0, 100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000]
        label = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]
        plt.xticks(loc, label)

        pp = PdfPages('thread_scheduling_gantt.pdf')
        pp.savefig()

        pp.close()

    @staticmethod
    def process_deflected_flits_images():
        Writer = animation.writers['ffmpeg']
        writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

        df = pd.read_csv('deflected_flit_per_src.csv')
        _min = df.values.min()
        _max = df.values.max()
        max_sample = np.shape(df)[0]
        n_figures = 20

        df_map = pd.read_csv('running_core_per_src.csv')

        for i in range(0, n_figures):

            sum_row = df[i * int(max_sample / n_figures): (i + 1) * int(max_sample / n_figures)].sum()
            avg_dfpki = sum_row / int(max_sample / n_figures)
            arr = np.asarray(avg_dfpki)
            arr = np.reshape(arr, (8, 8))
            arr = np.flip(np.transpose(arr), 0)

            fig1, (ax1) = plt.subplots()

            im = ax1.imshow(arr, interpolation='none', vmin=_min, vmax=_max, animated=True)

            fig1.colorbar(im, shrink=0.5)
            ax1.set_xticks(np.arange(.5, 8, 1), minor=True)
            ax1.set_yticks(np.arange(.5, 8, 1), minor=True)
            ax1.set_xticklabels([-1, 0, 1, 2, 3, 4, 5, 6, 7])
            ax1.set_yticklabels([8, 7, 6, 5, 4, 3, 2, 1, 0])
            ax1.grid(which='minor', color='w', linestyle='-', linewidth=1)

            var = df_map.iloc[(int(i * int(max_sample / n_figures) + (i + 1) * int(max_sample / n_figures)) / 2)]
            arr_map = np.asarray(var)
            arr_map = np.reshape(arr_map, (8, 8))
            arr_map = np.flip(np.transpose(arr_map), 0)
            for (j, k), label in np.ndenumerate(arr_map):
                if label >= 0:
                    ax1.text(k, j, label, ha='center', va='center', color="white")

            pp = PdfPages('test.pdf')
            pp.savefig()

            pp.close()

    def __init__(self):
        print('Starting!')

        NoculatorAnalyzer.process_power_per_source()
        NoculatorAnalyzer.process_network_average_latency()

        # NoculatorAnalyzer.process_temperature_per_source()
        # NoculatorAnalyzer.process_average_temperatures(self)

        NoculatorAnalyzer.process_average_power_per_source(self)
        NoculatorAnalyzer.process_deflected_flit_per_src(self)
        NoculatorAnalyzer.process_l1_miss_rate(self)
        NoculatorAnalyzer.process_memory_access_per_kilo_instruction(self)
        NoculatorAnalyzer.process_memory_access_per_src(self)
        NoculatorAnalyzer.process_core_progress_log_retired(self)
        NoculatorAnalyzer.process_memory_committed_instructions_per_source(self)
        NoculatorAnalyzer.process_core_cache_slice_access(self)
        NoculatorAnalyzer.process_cache_slice_mem_access()
        NoculatorAnalyzer.process_thread_scheduling_result(self)
        NoculatorAnalyzer.process_deflected_flits_images()

        print('Ready!')


if __name__ == '__main__':
    NoculatorAnalyzer()
